# Cv Prototype using Bazel

## Build Instructions
1. Install [bazel](https://docs.bazel.build/versions/4.1.0/install.html)
2. Run `bazel build //...` to build the entire repository.
3. Run `bazel test //...` to run all unit tests
4. Run the "chipper" package, `bazel run //chipper:chipper` (or just `... //chipper`) as there's only 
one `*_binary` rule in that package.
5. Build the Docker image and push it `bazel run //chipper:push`. This will fail because client credentials for GS Nexus don't exist.

## Notes
* Bazel's default Python rules assume that the same version of a library is 
going to be used for the entire monorepo. Hence, the loading of `requirements.txt`
in the `WORKSPACE` file. It is possible to specify separate requirements files
for each Bazel package (e.g., library, microservice), but it doesn't scale well
and takes up extra disk space. See the [this](https://github.com/bazelbuild/rules_python#installing-pip-dependencies)
for details.
* The NGA requires pulling base images from very specific repos which have
hardened images. With this in mind, this base image is pulled in the `WORKSPACE`
(or would be if we knew what we wanted, right now there is a placeholder) and
augmented by code in the `base-images` package. 
* Plugin for PyCharm/IntelliJ is alright but lags behind the latest executable version.
VSCode has a plugin, but I couldn't test it as it won't run on my Udev box.

## Reference
* https://github.com/bazelbuild/rules_python
* https://github.com/bazelbuild/rules_docker
* https://github.com/bazelbuild/rules_docker/tree/master/testing/examples 
(how to translate Dockerfiles into rules, small example in `base-images`)

## TODOS
* Need to figure out how to supply client config for Docker push (docker on
my udev machine won't use the `--config` argument, client.json _has_ to be in ~/.docker)
* Probably want to switch from Docker to podman to make CI/CD process easier.
* We will want to create some macros that can be imported to reduce the boilerplate that will be repeated for each microservice.   
* Need to figure out how to configure certain things (image tags, etc.) for dev builds vs Gitlab builds on PR merge. 
  But it looks like there are probably a few ways to handle this (configurable build attributes, or .bazelrc or --workspace_status_command).
* Need to figure out how to determine which targets to run to push images to Nexus. 
  [This](https://stackoverflow.com/questions/55578905/how-to-know-all-bazel-targets-affected-by-a-git-commit/55615172#55615172) 
  could help with that.
* There are a few TODOs embedded within the files.
